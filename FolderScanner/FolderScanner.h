//***		pre-include defines

#define		_CRT_SECURE_NO_WARNINGS

//***		include

#include	<Windows.h>
//#include	<stdio.h>

//***		macro build config

#define		C_MEMORY_STANDART			0
#define		WIN_MEMORY_STANDART			1

//			Windows optimization

/*
#ifdef		_WIN32
	#define		MEMORY_MANAGMENT		WIN_MEMORY_STANDART
#else
	#define		MEMORY_MANAGMENT		C_MEMORY_STANDART
#endif
*/

#define		MEMORY_MANAGMENT			C_MEMORY_STANDART

//***		macro const

#define		TYPE_SC_FOLDER_EMPTY		(unsigned __int8)(0x00		)
#define		TYPE_SC_FOLDER_CONTAINER	(unsigned __int8)(0x01		)
#define		TYPE_SC_FILE				(unsigned __int8)(0x02		)
#define		TYPE_SC_TERMINATOR			(unsigned __int8)(0x04		)

#define		NODE_OPENED					(DWORD_PTR		)(0x00000000)
#define		NODE_SEALED					(DWORD_PTR		)(0x00000001)

#define		EVENT_SC_NEW_FILE			(unsigned __int8)(0x00		)
#define		EVENT_SC_NEW_FOLDER			(unsigned __int8)(0x01		)
#define		EVENT_SC_DEL_FILE			(unsigned __int8)(0x02		)
#define		EVENT_SC_DEL_FOLDER			(unsigned __int8)(0x03		)
#define		EVENT_SC_MOD_FILE			(unsigned __int8)(0x04		)

#define		GIGABYTE_SIZE				(double			)(0x40000000)

//in string format its "."
#define		NODE_CURR_FOLDER_TAG		(__int16		)(0x002E	)

//in string format its ".."
#define		NODE_PREV_FOLDER_TAG		(__int16		)(0x2E2E	)


//in string format its L"."
#define		NODE_CURR_FOLDER_TAGW		(__int32		)(0x0000002E)

//in string format its L".."
#define		NODE_FOLDERS_TAGW			(__int32		)(0x002E002E)

//***		macro defines

#if MEMORY_MANAGMENT == WIN_MEMORY_STANDART

	#define		DEFINE_PROCESS_HEAP\
		extern HANDLE	procHeap

	#define		DECLARE_PROCESS_HEAP\
				HANDLE procHeap = NULL;

	#define		HEAP_INIT\
		procHeap			= GetProcessHeap()

	#define		HEAP_ALLOC(inSize	)\
		HeapAlloc	(procHeap, 0, (inSize	))
	
	#define		HEAP_FREE(inHandle	)\
		HeapFree	(procHeap, 0, (inHandle	))

#elif MEMORY_MANAGMENT == C_MEMORY_STANDART
	
	#define		DEFINE_PROCESS_HEAP
	#define		DECLARE_PROCESS_HEAP
	#define		HEAP_INIT

	#define		HEAP_ALLOC(inSize	)\
		malloc	(inSize		)
	
	#define		HEAP_FREE(inHandle	)\
		free	(inHandle	)

#endif

//***		types

typedef
	union StoreObject 
	
	uStoreObject, *huStoreObject;

//		Used for memory allocation
typedef
	struct FileObject
		{
			char*					name;
			union StoreObject*		next;
			unsigned __int8			type;

			LARGE_INTEGER			fileSize;
		}

	sFileObject, *hsFileObject;

//		Used for memory allocation
typedef
	struct FolderObject
		{
			char*					name;
			union StoreObject*		next;
			unsigned __int8			type;

			huStoreObject			nodes;
		}

	sFolderObject, *hsFolderObject;

//		Used for explicit convertation
union StoreObject
	{
		sFolderObject	fdo;
		sFileObject		flo;
	};

typedef
	struct ScanParams
		{
			char*				folderPath;
			HANDLE				findFirstResult;
			WIN32_FIND_DATAA	findFirstData;
			HANDLE				imageDataBase;
			LARGE_INTEGER*		respondSizeUI;
			char**				respondPathUI;
			HANDLE				mutexUI;
			unsigned __int8		reserved;		//Should be zero;
		}

	sScanParams, *hsScanParams;

typedef
	struct ScanResult
		{
			unsigned __int8		event;
			char*				path;
			char*				name;
			LARGE_INTEGER		prevSize;
			LARGE_INTEGER		currSize;
			struct ScanResult*	next;
		}

	sScanResult, *hsScanResult;

//***		globals

DEFINE_PROCESS_HEAP;

extern unsigned __int8	scanFinished;

//***		prototypes

huStoreObject 			buildScanImageA	(sScanParams	inArgs													);
void					freeScanImage	(huStoreObject	objects													);
huStoreObject			readScanImageA	(HANDLE			imageDataBase, char** hPath, unsigned __int8 reserved	);
hsScanResult			scanByImageA	(huStoreObject* inImage, sScanParams inArgs								);
huStoreObject			cloneImage		(huStoreObject inImage													);
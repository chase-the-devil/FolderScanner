#include <Shlobj.h>
#include "CForms.h"
#include "FolderScanner.h"
#include <stdio.h>

//***		UI variables

const unsigned		SCREEN_WIDTH	= 100;
const unsigned		SCREEN_HEIGHT	= 31;

TCApplication		scannerApp	(32);
TCMainMenu			menu		(C_TOP, 1, 177);
TCSubMenu			subMenu		(14, 5, 178, 32, &menu);
TCWindow			window		(100, 30, 0, 1, 32);
TCMemo				memo		(98, 10, 78, 100, 1, 1, 178, &window);
TCListBox			list		(98, 14, 1, 11, 32, &window);
TCButton			button		(98, 4, 1, 25, 178, 176, &window);

HANDLE				mutexUI			= CreateMutexA(NULL, 0, NULL);

LARGE_INTEGER		retSize			= {0};
char*				respondPath		= NULL;
void*				worker			= NULL;

//***		Working variables

huStoreObject		folderImage		= NULL;
hsScanResult		result			= NULL;
char*				currentPath		= NULL;

//***		Callback prototypes

void			FolderChoose	();
void			FolderScan		();
void			FileOpen		();
DWORD __stdcall progressUI		(LPVOID inParam);
DWORD __stdcall workerBuild		(LPVOID inParam);
DWORD __stdcall workerScan		(LPVOID inParam);
void			sendToMemo		(TCObject* SENDER, int Mx, int My);
void			openFolder		(TCObject* SENDER, int Mx, int My, DWORD BUTTON_RELEASED);

//***		Main

int main()
	{
		HEAP_INIT;

		list.ACTION_ON_MOUSE_DOWN	= sendToMemo;
		memo.ENABLED				= false;

		button.SET_CAPTION("Open folder", 43, 1);
		button.ACTION_ON_MOUSE_UP = openFolder;

		subMenu.PUSH("SCAN FOLDER", 178, 176, 1, FolderScan);
		subMenu.PUSH("BUILD IMAGE", 178, 176, 1, FolderChoose);
		subMenu.PUSH("LOAD IMAGE", 178, 176, 1, FileOpen);

		menu.ADD_TO_END(&subMenu, "FILE", 177);

		scannerApp.RUN();

		CloseHandle(mutexUI);

		return 0;
	}

//***		callbacks

void openFolder(TCObject* SENDER, int Mx, int My, DWORD BUTTON_RELEASED)
	{
		if(list.SELECTED && result)
			{
				int index = 0;

				hsScanResult curr = result;

				while(result && index < list.SELECTED->INDEX && index < list.GET_COUNT())
					{
						++index;
						curr = curr->next;
					}

				if(index == list.SELECTED->INDEX)
					ShellExecuteA(GetConsoleWindow(), "open", curr->path, NULL, NULL, SW_SHOWDEFAULT);
			}
	}

void FolderScan()
	{
		if(!worker)
			CreateThread(NULL, 0, workerScan, NULL, 0, NULL);
	}

void sendToMemo(TCObject* SENDER, int Mx, int My)
	{
		if(list.SELECTED && result)
			{
				int index = 0;

				hsScanResult curr = result;

				while(result && index < list.SELECTED->INDEX && index < list.GET_COUNT())
					{
						++index;
						curr = curr->next;
					}

				if(index == list.SELECTED->INDEX)
					{
						memo.SET_TEXT("Path :");
						memo.ADD_TEXT(curr->path);

						if(curr->event == EVENT_SC_MOD_FILE)
							{
								char number[35];

								memo.ADD_LINES(0);
								sprintf(number, "Previous size :%d bytes", curr->prevSize.QuadPart);
								memo.ADD_TEXT(number);

								memo.ADD_LINES(0);
								sprintf(number, "Current size :%d bytes", curr->currSize.QuadPart);
								memo.ADD_TEXT(number);
							}
						else if(curr->event == EVENT_SC_DEL_FILE || curr->event == EVENT_SC_NEW_FILE)
							{
								char number[35];

								memo.ADD_LINES(0);
								sprintf(number, "File size :%d bytes", curr->prevSize.QuadPart);
								memo.ADD_TEXT(number);
							}

						memo.SHOW_BUFFER();
					}
			}
	}

DWORD __stdcall workerScan(LPVOID inParam)
	{
		if(!currentPath)
			return -1;

		sScanParams			inParams;
		
		retSize.QuadPart				= 0;

		inParams.folderPath				= currentPath;
		inParams.mutexUI				= mutexUI;
		inParams.respondPathUI			= &respondPath;
		inParams.respondSizeUI			= &retSize;
		inParams.findFirstResult		= FindFirstFileA(currentPath, &inParams.findFirstData);
		inParams.reserved				= 0;

		scanFinished					= 0;

		CreateThread(NULL, 0, progressUI, &inParams, 0, NULL);

		huStoreObject copy = cloneImage(folderImage);

		hsScanResult tmp;
		while(result)
			{
				if(result->name)
					HEAP_FREE(result->name);

				if(result->path)
					HEAP_FREE(result->path);

				tmp = result->next;

				HEAP_FREE(result);

				result = tmp;
			}

		list.CLEAR();
		list.UPDATE_LIST();

		result = scanByImageA(&copy, inParams);

		currentPath[strlen(currentPath)] = '\*';

		if(result)
			{
				hsScanResult tmp = result;
				char buffer[500];

				memo.CLEAR();
				memo.SHOW_BUFFER();

				while(tmp)
					{
						switch(tmp->event)
							{
								case EVENT_SC_NEW_FILE:
									{
										strcpy(buffer,	"File   created  :");
										break;
									}
								case EVENT_SC_NEW_FOLDER:
									{
										strcpy(buffer,	"Folder created  :");
										break;
									}
								case EVENT_SC_MOD_FILE:
									{
										strcpy(buffer,	"File   modified :");
										break;
									}
								case EVENT_SC_DEL_FILE:
									{
										strcpy(buffer,	"File   deleted  :");
										break;
									}
								case EVENT_SC_DEL_FOLDER:
									strcpy(buffer,		"Folder deleted  :");
							}
								
						strcat(buffer, tmp->name);

						list.ADD_TO_END(buffer, 178, 32, 3);

						tmp = tmp->next;
					}

				list.UPDATE_LIST();
			}
		else
			{
				list.CLEAR();
				list.UPDATE_LIST();

				memo.SET_TEXT("Nothing changed!");
				memo.SHOW_BUFFER();
			}

		FindClose(inParams.findFirstResult);

		worker = NULL;

		return 0;
	}

void FolderChoose()
	{
		if(!worker)
			worker = CreateThread(NULL, 0, workerBuild, NULL, 0, NULL);
	}

DWORD __stdcall workerBuild(LPVOID inParam)
	{
		BROWSEINFOA BI;
		ITEMIDLIST* pItem;

		BI.hwndOwner = GetConsoleWindow();
		BI.lpfn = NULL;
		BI.lpszTitle = "Choose folder to scan";
		BI.pidlRoot = NULL;
		BI.pszDisplayName = (LPSTR)SHAlloc(MAX_PATH);
		BI.ulFlags = 0;
		BI.lParam = NULL;

		if(pItem = SHBrowseForFolderA(&BI))
			{
				SHGetPathFromIDListA(pItem, BI.pszDisplayName);

				OPENFILENAMEA	params;

				params.lStructSize			= sizeof(OPENFILENAME);
				params.hInstance			= NULL;
				params.lCustData			= NULL;
				params.lpfnHook				= NULL;
				params.lpTemplateName		= NULL;
				params.lpstrCustomFilter	= NULL;
				params.nMaxCustFilter		= 0;
				params.lpstrFileTitle		= NULL;
				params.nMaxFileTitle		= 0;

				params.hwndOwner			= GetConsoleWindow();

				params.lpstrFile			= new char[5000];
				*params.lpstrFile			= 0;

				params.nMaxFile				= 5000;
				params.lpstrInitialDir		= NULL;
				params.lpstrTitle			= "Choose location to save Data Base Image";
				params.lpstrFilter			= "Data Base Image | *.db\0*.db\0\0";
				params.lpstrDefExt			= "db";
				params.FlagsEx				= 0;
				params.Flags				= 0;
				params.nFilterIndex			= 0;

				if(GetSaveFileNameA(&params))
					{
						list.CLEAR();
						list.UPDATE_LIST();

						unsigned int		len = strlen(BI.pszDisplayName);
						if(BI.pszDisplayName[len - 1] == '\\')
							len += 2;
						else
							len += 3;

						char*				path		= new char[len];

						strcpy(path, BI.pszDisplayName);
						
						path[len - 1] = 0;
						path[len - 2] = '\*';
						path[len - 3] = '\\';

						if(currentPath)
							delete[] currentPath;

						currentPath = path;

						HANDLE				db			= CreateFileA(params.lpstrFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
						sScanParams			inParams;
		
						retSize.QuadPart				= 0;

						inParams.folderPath				= path;
						inParams.imageDataBase			= db;
						inParams.mutexUI				= mutexUI;
						inParams.respondPathUI			= &respondPath;
						inParams.respondSizeUI			= &retSize;
						inParams.findFirstResult		= FindFirstFileA(path, &inParams.findFirstData);
						inParams.reserved				= 0;

						scanFinished					= 0;

						CreateThread(NULL, 0, progressUI, &inParams, 0, NULL);

						if(folderImage)
							freeScanImage(folderImage);

						folderImage						= buildScanImageA(inParams);

						path[len - 2] = '\*';

						FindClose(inParams.findFirstResult);

						CloseHandle(db);

						memo.SET_TEXT("Image built!");
						memo.SHOW_BUFFER();
					}

				delete[] params.lpstrFile;
			}

		SHFree(pItem);
		SHFree(BI.pszDisplayName);

		worker = NULL;

		return 0;
	}

DWORD __stdcall progressUI(LPVOID inParam)
	{
		char* prev = NULL;
		char numBuffer[50];
		char* path = NULL;

	__loop:

		WaitForSingleObject(mutexUI, INFINITE);

		if(scanFinished)
			{
				ReleaseMutex(mutexUI);
				return 0;
			}

		if(((hsScanParams)(inParam))->respondSizeUI)
			{
				sprintf(numBuffer, "Scanned size: %f", ((hsScanParams)(inParam))->respondSizeUI->QuadPart / GIGABYTE_SIZE);

				memo.SET_TEXT(numBuffer);
				memo.ADD_LINES(0);
			}

		if(*((hsScanParams)(inParam))->respondPathUI && prev != *((hsScanParams)(inParam))->respondPathUI)
			{	
				if(path)
					delete[] path;

				path = new char[strlen(*((hsScanParams)(inParam))->respondPathUI) + 1];
				CharToOemA(*((hsScanParams)(inParam))->respondPathUI, path);
			}

		if(path)
			memo.ADD_TEXT(path);

		memo.SHOW_BUFFER();

		ReleaseMutex(mutexUI);

		Sleep(90);

		goto __loop;
	}

void FileOpen()
	{
		OPENFILENAMEA	params;

		params.lStructSize			= sizeof(OPENFILENAME);
		params.hInstance			= NULL;
		params.lCustData			= NULL;
		params.lpfnHook				= NULL;
		params.lpTemplateName		= NULL;
		params.lpstrCustomFilter	= NULL;
		params.nMaxCustFilter		= 0;
		params.lpstrFileTitle		= NULL;
		params.nMaxFileTitle		= 0;

		params.hwndOwner			= GetConsoleWindow();

		params.lpstrFile			= new char[5000];
		*params.lpstrFile			= 0;

		params.nMaxFile				= 5000;
		params.lpstrInitialDir		= NULL;
		params.lpstrTitle			= "Choose Data Base Image to open";
		params.lpstrFilter			= "Data Base Image | *.db\0*.db\0\0";
		params.lpstrDefExt			= "db";
		params.FlagsEx				= 0;
		params.Flags				= 0;
		params.nFilterIndex			= 0;

		if(GetOpenFileNameA(&params))
			{
				list.CLEAR();
				list.UPDATE_LIST();

				HANDLE db			= CreateFileA(params.lpstrFile, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

				if(folderImage)
					freeScanImage(folderImage);

				if(currentPath)
					delete[] currentPath;

				folderImage = readScanImageA(db, &currentPath, 0);

				CloseHandle(db);

				memo.SET_TEXT("Image loaded!");
				memo.ADD_LINES(0);
				memo.ADD_TEXT(params.lpstrFile);
				memo.SHOW_BUFFER();
			}

		delete[] params.lpstrFile;
	}
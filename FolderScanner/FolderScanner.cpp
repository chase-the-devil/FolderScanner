#include "FolderScanner.h"

DECLARE_PROCESS_HEAP;

unsigned __int8	scanFinished =	0;

huStoreObject buildScanImageA(sScanParams inArgs)
	{	
		DWORD				bytesWritten;
		huStoreObject		headRetList		= NULL;
		huStoreObject		nextRetList		= NULL;

		unsigned int		folderPathLen	= strlen(inArgs.folderPath);

		//For UI thread
		WaitForSingleObject(inArgs.mutexUI, INFINITE);

		if(!inArgs.reserved)
			{
				scanFinished = 0;
				unsigned short	len = strlen(inArgs.folderPath) + 1;

				WriteFile(inArgs.imageDataBase, &len, sizeof(unsigned short), &bytesWritten, NULL);
				WriteFile(inArgs.imageDataBase, inArgs.folderPath, strlen(inArgs.folderPath) + 1, &bytesWritten, NULL);
			}

		*inArgs.respondPathUI = inArgs.folderPath;

		ReleaseMutex(inArgs.mutexUI);

		inArgs.folderPath[--folderPathLen]	= 0;

		if(inArgs.findFirstResult != INVALID_HANDLE_VALUE)
			{
				do
					{
						if(inArgs.findFirstData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
							{
								if	( 
										!*inArgs.findFirstData.cFileName									||
										(
											*((__int16*)(inArgs.findFirstData.cFileName)) 					!= 
											NODE_CURR_FOLDER_TAG				&&
											(*((__int32*)(inArgs.findFirstData.cFileName)) & 0x00ffffff) 	!= 
											NODE_PREV_FOLDER_TAG
										)
									)
									{
										nextRetList						= headRetList;
										headRetList						= (huStoreObject)HEAP_ALLOC(sizeof(sFolderObject));

										headRetList->fdo.next			= nextRetList;
										headRetList->fdo.nodes			= NULL;

										unsigned int	len				= strlen(inArgs.findFirstData.cFileName) + 1;
										unsigned short	bytes			= len * sizeof(char);

										headRetList->fdo.name			= (char*)HEAP_ALLOC(bytes);
										strcpy(headRetList->fdo.name, inArgs.findFirstData.cFileName);

										char* nextFolder				= NULL;

										if(inArgs.findFirstData.dwFileAttributes & FILE_ATTRIBUTE_REPARSE_POINT)
											goto __Link;

										len								+= folderPathLen + 2;
										nextFolder						= (char*)HEAP_ALLOC(len * sizeof(char));

										strcpy(nextFolder, inArgs.folderPath);
										strcat(nextFolder, inArgs.findFirstData.cFileName);

										nextFolder[--len	]			= 0;
										nextFolder[--len	]			= '*';
										nextFolder[--len	]			= '\\';

										sScanParams	newParams;

										newParams.findFirstResult		= FindFirstFileA(nextFolder, &newParams.findFirstData);

										if(newParams.findFirstResult != INVALID_HANDLE_VALUE)
											{
												newParams.folderPath			= nextFolder;
												newParams.imageDataBase			= inArgs.imageDataBase;
												newParams.mutexUI				= inArgs.mutexUI;
												newParams.respondPathUI			= inArgs.respondPathUI;
												newParams.respondSizeUI			= inArgs.respondSizeUI;
												newParams.reserved				= 1;

												headRetList->fdo.type			= TYPE_SC_FOLDER_CONTAINER;

												WriteFile	(
																inArgs.imageDataBase,
																&(headRetList->fdo.type),
																sizeof(headRetList->fdo.type),
																&bytesWritten, 
																NULL
															);

												WriteFile	(
																inArgs.imageDataBase,
																&bytes,
																sizeof(bytes),
																&bytesWritten, 
																NULL
															);

												WriteFile	(
																inArgs.imageDataBase,
																headRetList->fdo.name,
																bytes - sizeof(char), 
																&bytesWritten, 
																NULL
															);

												headRetList->fdo.nodes			= buildScanImageA(newParams);

												FindClose(newParams.findFirstResult);
											}
										else
											{
	__Link:

												headRetList->fdo.type					= TYPE_SC_FOLDER_EMPTY;

												WriteFile	(
																inArgs.imageDataBase,
																&(headRetList->fdo.type),
																sizeof(headRetList->fdo.type),
																&bytesWritten, 
																NULL
															);

												WriteFile	(
																inArgs.imageDataBase,
																&bytes,
																sizeof(bytes),
																&bytesWritten, 
																NULL
															);

												WriteFile	(
																inArgs.imageDataBase,
																headRetList->fdo.name,
																bytes - sizeof(char), 
																&bytesWritten, 
																NULL
															);
											}

										if(nextFolder)
											HEAP_FREE(nextFolder);
									}
							} 
						else
							{
								nextRetList							= headRetList;
								headRetList							= (huStoreObject)HEAP_ALLOC(sizeof(sFileObject));

								headRetList->flo.next				= nextRetList;
								headRetList->flo.type				= TYPE_SC_FILE;
								headRetList->flo.fileSize.LowPart	= inArgs.findFirstData.nFileSizeLow;
								headRetList->flo.fileSize.HighPart	= inArgs.findFirstData.nFileSizeHigh;

								WaitForSingleObject(inArgs.mutexUI, INFINITE);

								(*inArgs.respondSizeUI).QuadPart	+=	headRetList->flo.fileSize.QuadPart;

								ReleaseMutex(inArgs.mutexUI);

								unsigned int	len					= strlen(inArgs.findFirstData.cFileName) + 1;
								unsigned short	bytes				= len * sizeof(char);

								headRetList->flo.name				= (char*)HEAP_ALLOC(len);
								strcpy(headRetList->flo.name, inArgs.findFirstData.cFileName);

								//linear tree writing to file

								WriteFile	(
												inArgs.imageDataBase,
												&(headRetList->flo.type),
												sizeof(headRetList->flo.type), 
												&bytesWritten, 
												NULL
											);

								WriteFile	(
												inArgs.imageDataBase,
												&(headRetList->flo.fileSize.QuadPart),
												sizeof(headRetList->flo.fileSize.QuadPart), 
												&bytesWritten, 
												NULL
											);

								WriteFile	(
												inArgs.imageDataBase,
												&bytes,
												sizeof(bytes),
												&bytesWritten, 
												NULL
											);

								WriteFile	(
												inArgs.imageDataBase,
												headRetList->flo.name,
												bytes - sizeof(char), 
												&bytesWritten,
												NULL
											);
							}
					}
				while(FindNextFileA(inArgs.findFirstResult, &inArgs.findFirstData));

				unsigned __int8 terminator = TYPE_SC_TERMINATOR;

				WriteFile	(
								inArgs.imageDataBase,
								&(terminator),
								sizeof(terminator),
								&bytesWritten, 
								NULL
							);
			}

		//For UI thread
		WaitForSingleObject(inArgs.mutexUI, INFINITE);

		*inArgs.respondPathUI = NULL;

		if(!inArgs.reserved)
			scanFinished = 1;

		ReleaseMutex(inArgs.mutexUI);

		return headRetList;
	}

huStoreObject readScanImageA(HANDLE imageDataBase, char** hPath, unsigned __int8 reserved)
	{
		unsigned __int8 objType;
		unsigned short	nameLen;
		DWORD			bytesRead;
		huStoreObject	lst			= NULL;
		huStoreObject	curr		= NULL;

		if(!reserved)
			{
				ReadFile(imageDataBase, &nameLen, sizeof(unsigned short), &bytesRead, NULL);

				*hPath = (char*)HEAP_ALLOC(nameLen);
				ReadFile(imageDataBase, *hPath, nameLen, &bytesRead, NULL);
			}

		while(ReadFile(imageDataBase, &objType, sizeof(unsigned __int8), &bytesRead, NULL))
			{
				if(objType == TYPE_SC_TERMINATOR)
					{
						if(curr)
							curr->flo.next = NULL;

						return lst;
					}

				if(objType == TYPE_SC_FILE)
					{
						if(!curr)
							{
								curr			= (huStoreObject)HEAP_ALLOC(sizeof(sFileObject));
								lst				= curr;
							}
						else
							{
								curr->flo.next	= (huStoreObject)HEAP_ALLOC(sizeof(sFileObject));
								curr			= curr->flo.next;
							}

						curr->flo.type		= objType;

						ReadFile	(
										imageDataBase, 
										&curr->flo.fileSize.QuadPart, 
										sizeof(curr->flo.fileSize.QuadPart),
										&bytesRead, 
										NULL
									);
								
						ReadFile	(
										imageDataBase, 
										&nameLen, 
										sizeof(nameLen),
										&bytesRead, 
										NULL
									);

						curr->flo.name		= (char*)HEAP_ALLOC(nameLen);

						ReadFile	(
										imageDataBase, 
										curr->flo.name, 
										nameLen - sizeof(char), 
										&bytesRead, 
										NULL
									);

						*((char*)((DWORD_PTR)(curr->flo.name) + nameLen - sizeof(char))) = 0;

						continue;
					}

				if(!curr)
					{
						curr			= (huStoreObject)HEAP_ALLOC(sizeof(sFolderObject));
						lst				= curr;
					}
				else
					{
						curr->fdo.next	= (huStoreObject)HEAP_ALLOC(sizeof(sFolderObject));
						curr			= curr->fdo.next;
					}

				curr->fdo.type			= objType;

				ReadFile	(
								imageDataBase, 
								&nameLen, 
								sizeof(nameLen),
								&bytesRead, 
								NULL
							);

				curr->fdo.name		= (char*)HEAP_ALLOC(nameLen);

				ReadFile	(
								imageDataBase, 
								curr->fdo.name, 
								nameLen - sizeof(char), 
								&bytesRead, 
								NULL
							);

				*((char*)((DWORD_PTR)(curr->fdo.name) + nameLen - sizeof(char))) = 0;

				if(curr->fdo.type == TYPE_SC_FOLDER_CONTAINER)
					{
						curr->fdo.nodes = readScanImageA(imageDataBase, NULL, 1);

						continue;
					}
			}

		freeScanImage(lst);

		return NULL;
	}

//Deleting nodes in recursive call; need to pass nodes like huStoreObject*

hsScanResult scanByImageA(huStoreObject* inImage, sScanParams inArgs)
	{
		hsScanResult		localList		= NULL;
		hsScanResult		listCurr		= NULL;

		unsigned int		folderPathLen	= strlen(inArgs.folderPath);
		
		inArgs.folderPath[--folderPathLen]	= 0;

		//For UI thread
		WaitForSingleObject(inArgs.mutexUI, INFINITE);

		*inArgs.respondPathUI = inArgs.folderPath;

		if(!inArgs.reserved)
			scanFinished = 0;

		ReleaseMutex(inArgs.mutexUI);

		if(inArgs.findFirstResult != INVALID_HANDLE_VALUE)
			{
				do
					{
						if(inArgs.findFirstData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
							{
								if	( 
										!*inArgs.findFirstData.cFileName				||
										(
											*((__int16*)(inArgs.findFirstData.cFileName)) != 
											NODE_CURR_FOLDER_TAG				&&
											*((__int16*)(inArgs.findFirstData.cFileName)) != 
											NODE_PREV_FOLDER_TAG
										)
									)
									{	
										huStoreObject prev = NULL;
										huStoreObject curr = *inImage;

										while(curr)
											{
												if	(
														curr->fdo.type <= TYPE_SC_FOLDER_CONTAINER &&
														!strcmp(curr->fdo.name, inArgs.findFirstData.cFileName)
													)
													{
														if(curr->fdo.type == TYPE_SC_FOLDER_CONTAINER)
															{
																unsigned int	len				= strlen(inArgs.findFirstData.cFileName) + folderPathLen + 3;
																char*			nextFolder		= (char*)HEAP_ALLOC(len * sizeof(char));

																strcpy(nextFolder, inArgs.folderPath);
																strcat(nextFolder, inArgs.findFirstData.cFileName);

																nextFolder[--len	]			= 0;
																nextFolder[--len	]			= '*';
																nextFolder[--len	]			= '\\';

																sScanParams		newParams;

																newParams.findFirstResult		= FindFirstFileA(nextFolder, &newParams.findFirstData);

																if(newParams.findFirstResult != INVALID_HANDLE_VALUE)
																	{
																		newParams.folderPath			= nextFolder;
																		//newParams.imageDataBase		= inArgs.imageDataBase;
																		newParams.mutexUI				= inArgs.mutexUI;
																		newParams.respondPathUI			= inArgs.respondPathUI;
																		newParams.respondSizeUI			= inArgs.respondSizeUI;
																		newParams.reserved				= 1;

																		if(listCurr)
																			listCurr->next				= scanByImageA(&curr->fdo.nodes, newParams);
																		else
																			{
																				listCurr				= scanByImageA(&curr->fdo.nodes, newParams);
																				localList				= listCurr;
																			}

																		//optimize later

																		if(listCurr)
																			{
																				while(listCurr->next)
																					listCurr = listCurr->next;
																			}

																		FindClose(newParams.findFirstResult);
																	}
																else
																	{
																		huStoreObject iter = curr->fdo.nodes;

																		while(iter)
																			{
																				if(listCurr)
																					{
																						listCurr->next	= (hsScanResult)HEAP_ALLOC(sizeof(sScanResult));
																						listCurr		= listCurr->next;
																					}
																				else
																					{
																						listCurr		= (hsScanResult)HEAP_ALLOC(sizeof(sScanResult));
																						localList		= listCurr;
																					}

																				if(iter->flo.type == TYPE_SC_FILE)
																					{
																						listCurr->event		= EVENT_SC_DEL_FILE;
																						listCurr->prevSize	= iter->flo.fileSize;
																					}
																				else //if(iter->fdo.type <= TYPE_SC_FOLDER_CONTAINER)
																					listCurr->event		= EVENT_SC_DEL_FOLDER;

																				unsigned int nameLen	= strlen(iter->flo.name) + 1;

																				listCurr->name			= (char*)HEAP_ALLOC(nameLen);
																				strcpy(listCurr->name, iter->flo.name);

																				listCurr->path			= (char*)HEAP_ALLOC(folderPathLen + 1);
																				strcpy(listCurr->path, inArgs.folderPath);

																				iter = iter->fdo.next;
																			}
																	}

																HEAP_FREE(nextFolder);
															}

														//remove from list and free
														if(prev)
															prev->fdo.next = curr->fdo.next;
														else
															*inImage = curr->fdo.next;

														curr->fdo.next = NULL;
														freeScanImage(curr);

														break;
													}

												prev = curr;
												curr = curr->fdo.next;
											}

										//event new curr folder if curr
										if(!curr)
											{
												if(listCurr)
													{
														listCurr->next	= (hsScanResult)HEAP_ALLOC(sizeof(sScanResult));
														listCurr		= listCurr->next;
													}
												else
													{
														listCurr		= (hsScanResult)HEAP_ALLOC(sizeof(sScanResult));
														localList		= listCurr;
													}

												listCurr->event			= EVENT_SC_NEW_FOLDER;

												unsigned int nameLen	= strlen(inArgs.findFirstData.cFileName) + 1;

												listCurr->name			= (char*)HEAP_ALLOC(nameLen);
												strcpy(listCurr->name, inArgs.findFirstData.cFileName);

												listCurr->path			= (char*)HEAP_ALLOC(folderPathLen + 1);
												strcpy(listCurr->path, inArgs.folderPath);
											}
									}
							} 
						else
							{
								WaitForSingleObject(inArgs.mutexUI, INFINITE);

								LARGE_INTEGER	num;

								num.LowPart		= inArgs.findFirstData.nFileSizeLow;
								num.HighPart	= inArgs.findFirstData.nFileSizeHigh;

								(*inArgs.respondSizeUI).QuadPart	+=	num.QuadPart;

								ReleaseMutex(inArgs.mutexUI);

								huStoreObject prev = NULL;
								huStoreObject curr = *inImage;

								while(curr)
									{
										if	(
												curr->fdo.type == TYPE_SC_FILE &&
												!strcmp(curr->flo.name, inArgs.findFirstData.cFileName)
											)
											{
												if(num.QuadPart != curr->flo.fileSize.QuadPart)
													{
														if(listCurr)
															{
																listCurr->next	= (hsScanResult)HEAP_ALLOC(sizeof(sScanResult));
																listCurr		= listCurr->next;
															}
														else
															{
																listCurr		= (hsScanResult)HEAP_ALLOC(sizeof(sScanResult));
																localList		= listCurr;
															}

														listCurr->event			= EVENT_SC_MOD_FILE;
														listCurr->prevSize		= num;
														listCurr->currSize		= curr->flo.fileSize;

														unsigned int nameLen	= strlen(curr->flo.name) + 1;

														listCurr->name			= (char*)HEAP_ALLOC(nameLen);
														strcpy(listCurr->name, curr->flo.name);

														listCurr->path			= (char*)HEAP_ALLOC(folderPathLen + 1);
														strcpy(listCurr->path, inArgs.folderPath);
													}

												//remove from list
												if(prev)
													prev->flo.next = curr->flo.next;
												else
													*inImage = curr->flo.next;

												curr->flo.next = NULL;
												freeScanImage(curr);

												break;
											}

										prev = curr;
										curr = curr->flo.next;
									}

								//event new current file if !curr
								if(!curr)
									{
										if(listCurr)
											{
												listCurr->next	= (hsScanResult)HEAP_ALLOC(sizeof(sScanResult));
												listCurr		= listCurr->next;
											}
										else
											{
												listCurr		= (hsScanResult)HEAP_ALLOC(sizeof(sScanResult));
												localList		= listCurr;
											}

										listCurr->event			= EVENT_SC_NEW_FILE;
										listCurr->prevSize		= num;

										unsigned int nameLen	= strlen(inArgs.findFirstData.cFileName) + 1;

										listCurr->name			= (char*)HEAP_ALLOC(nameLen);
										strcpy(listCurr->name, inArgs.findFirstData.cFileName);

										listCurr->path					= (char*)HEAP_ALLOC(folderPathLen + 1);
										strcpy(listCurr->path, inArgs.folderPath);
									}
							}
					}
				while(FindNextFileA(inArgs.findFirstResult, &inArgs.findFirstData));
			}

		huStoreObject curr = *inImage;

		//all others in image are deleted
		while(curr)
			{
				if(listCurr)
					{
						listCurr->next	= (hsScanResult)HEAP_ALLOC(sizeof(sScanResult));
						listCurr		= listCurr->next;
					}
				else
					{
						listCurr		= (hsScanResult)HEAP_ALLOC(sizeof(sScanResult));
						localList		= listCurr;
					}

				if(curr->flo.type == TYPE_SC_FILE)
					{
						listCurr->event			= EVENT_SC_DEL_FILE;
						listCurr->prevSize		= curr->flo.fileSize;

						unsigned int nameLen	= strlen(curr->flo.name) + 1;

						listCurr->name			= (char*)HEAP_ALLOC(nameLen);
						strcpy(listCurr->name, curr->flo.name);

						listCurr->path			= (char*)HEAP_ALLOC(folderPathLen + 1);
						strcpy(listCurr->path, inArgs.folderPath);
					}
				else
					{
						listCurr->event			= EVENT_SC_DEL_FOLDER;

						unsigned int nameLen	= strlen(curr->fdo.name) + 1;

						listCurr->name			= (char*)HEAP_ALLOC(nameLen);
						strcpy(listCurr->name, curr->fdo.name);

						listCurr->path			= (char*)HEAP_ALLOC(folderPathLen + 1);
						strcpy(listCurr->path, inArgs.folderPath);
					}

				curr = curr->flo.next;
			}

		freeScanImage(*inImage);
		*inImage = NULL;

		//For UI thread
		WaitForSingleObject(inArgs.mutexUI, INFINITE);

		*inArgs.respondPathUI = NULL;

		if(!inArgs.reserved)
			scanFinished = 1;

		ReleaseMutex(inArgs.mutexUI);

		if(listCurr)
			listCurr->next	= NULL;

		return localList;
	}

huStoreObject cloneImage(huStoreObject inImage)
	{
		huStoreObject result = NULL;
		huStoreObject curr	 = NULL;

		while(inImage)
			{
				if(inImage->flo.type == TYPE_SC_FILE)
					{
						if(result)
							{
								curr->flo.next = (huStoreObject)HEAP_ALLOC(sizeof(sFileObject));
								curr = curr->flo.next;
							}
						else
							{
								curr = (huStoreObject)HEAP_ALLOC(sizeof(sFileObject));
								result = curr;
							}

						curr->flo.type = inImage->flo.type;
						curr->flo.fileSize = inImage->flo.fileSize;
						curr->flo.name = (char*)HEAP_ALLOC(strlen(inImage->flo.name) + 1);
						strcpy(curr->flo.name, inImage->flo.name);
						curr->flo.next = NULL;
					}
				else
					{
						if(result)
							{
								curr->fdo.next = (huStoreObject)HEAP_ALLOC(sizeof(sFolderObject));
								curr = curr->fdo.next;
							}
						else
							{
								curr = (huStoreObject)HEAP_ALLOC(sizeof(sFolderObject));
								result = curr;
							}

						curr->fdo.type = inImage->fdo.type;
						curr->fdo.name = (char*)HEAP_ALLOC(strlen(inImage->fdo.name) + 1);
						strcpy(curr->fdo.name, inImage->fdo.name);

						if(curr->fdo.type != TYPE_SC_FOLDER_EMPTY)
							curr->fdo.nodes = cloneImage(inImage->fdo.nodes);
						else
							curr->fdo.nodes = NULL;

						curr->fdo.next = NULL;
					}

				inImage = inImage->flo.next;
			}

		if(curr)
			curr->flo.next = NULL;

		return result;
	}

void freeScanImage(huStoreObject objects)
	{
		huStoreObject tmp;

		while(objects)
			{
				if(objects->fdo.type & TYPE_SC_FOLDER_CONTAINER)
					freeScanImage(objects->fdo.nodes);

				if(objects->flo.name)
					HEAP_FREE(objects->flo.name);

				tmp = objects->flo.next;

				HEAP_FREE(objects);

				objects = tmp;
			}
	}